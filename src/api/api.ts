/* Core */
import waait from 'waait';

import { DayModel } from '../types/DayModel';

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
    async getWeather() {
        
        const response = await fetch(`${WEATHER_API_URL}`, { method: 'GET' });
        const result: DayModel[] = await response.json();
        await waait(1000);

        return result;
    },
};
