/* Core */
import { createContext, useState, SetStateAction } from 'react';

export const Context = createContext<SelectedDayProvideShape>([
    null,
    () => null,
]);

export const SelectedDayProvider: React.FC = props => {
    const state = useState<string | null>('55634253-cfc5-40cd-abc2-73d27c704d33');

    return <Context.Provider value={state}>{props.children}</Context.Provider>;
};


/* Types */
type SelectedDayProvideShape = [
    string | null,
    React.Dispatch<SetStateAction<string | null>>,
];