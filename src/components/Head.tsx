/* Instruments */
import { longWeekDay, longMonth, currentDay } from '../helpers';

export const Head = () => {
    const longWeekDayJSX = longWeekDay();
    const longMonthJSX = longMonth();
    const currentDayJSX = currentDay();

    return (
        <>
            <div className="head">
                <div className="icon rainy"></div>
                <div className="current-date">
                    <p>{ longWeekDayJSX }</p>
                    <span>{ `${currentDayJSX} ${longMonthJSX}` }</span>
                </div>
            </div>
        </>
    );
};