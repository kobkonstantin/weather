export const CurrentWeather = () => {
    return (
        <>
            <div className="current-weather">
                <p className="temperature">16</p>
                <p className="meta">
                    <span className="rainy">% 89</span>
                    <span className="humidity">% 73</span>
                </p>
            </div>
        </>
    );
};