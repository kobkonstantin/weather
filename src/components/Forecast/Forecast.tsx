/* Core */
import { useContext } from 'react';

/* Components */
import  { Day } from './Day';

/* Instruments */
import { Context } from '../../lib/selectedDayContext';
// import days from '../../mock-data/forecast.json';
import { useForecast } from '../../hooks/useForecast';

export const Forecast = () => {
    const [selectedDayId] = useContext(Context);
    
    const { data: days } = useForecast();

    const daysJSX = days.data?.map(day => {
        return <Day key={day.id} day={day} selected={selectedDayId === day.id} />
    }).slice(0, 6) ?? <h1>Загрузка...</h1>;

    return (
        <>
            <div className="forecast">
               { daysJSX }
            </div>
        </>
    );
};