import React from "react";
import { DayModel } from '../../types/DayModel';
import { longWeekDay } from '../../helpers';

export const Day: React.FC<DayProps> = (props) => {
    const showDay = new Date(props.day.day);
    const longWeekDayJSX = longWeekDay(showDay);

    return (
        <>
            <div className={`day ${props.day.type} ${props.selected ? "selected" : ""}`}>
                <p>{ longWeekDayJSX }</p>
                <span>{props.day.temperature}</span>
            </div>
        </>
    );
};

/* Types */
interface DayProps {
    day: DayModel;
    selected: boolean;
}