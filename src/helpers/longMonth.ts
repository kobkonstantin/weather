export const longMonth = (DateForShow?: Date) => {
    if (DateForShow === undefined) {
        DateForShow = new Date();
    }
    return new Intl.DateTimeFormat('ru-RU', { month: 'long' }).format(DateForShow);
};