export const longWeekDay = (DateForShow?: Date) => {
    if (DateForShow === undefined) {
        DateForShow = new Date();
    }
    return new Intl.DateTimeFormat('ru-RU', { weekday: 'long' }).format(DateForShow);
};