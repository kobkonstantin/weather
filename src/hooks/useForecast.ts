/* Core */
import { useQuery } from 'react-query';

/* Instuments */
import { api } from '../api';
import { DayModel } from '../types/DayModel';

export const useForecast = () => {
    
    const query = useQuery<DayModel[]>('days', api.getWeather);

    return query;
}